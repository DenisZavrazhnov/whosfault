# WhosFault

## Описание
# Данная программа предназначена для того, чтобы показать, что при OutOfMemoryException (далее "OOM"), не обязательно виноват тот класс, что отображается в stackTrace.
Программа содержит всего 3 класса.

1.MemoryGrabber c методом grabMemory()  
![alt text](https://i.ibb.co/hV9qVjG/grabber.jpg)

2.Culprit с методом init()  
![alt text](https://i.ibb.co/YNTkW5m/culprit.jpg)

3.Main - точка входа  
![alt text](https://i.ibb.co/JzLwHNq/main.png)

После запуска программы получаем ошибку:  
![alt text](https://i.ibb.co/RhbV8T9/ex.png)

Из стек терейса видим, что в строке 6 класса Main возникла ошибка, она ведет нас в класс Culprit (P.S. У вас stackTrace может отличаться, но будет вести в класс Culprit метод init())

Для того, чтобы понять действительно ли проблема в классе Culprit воспользуемся соззданым дампом хипа:  
![alt text](https://i.ibb.co/LQgPGjJ/d1.png)   
![alt text](https://i.ibb.co/ZLy6Qzt/d2.png)  

Изучить данный дамп можно с помощью Java VisualVM (находится вместе с jdk в папке bin)  

![alt text](https://i.ibb.co/VpCS0hT/1.png)  
![alt text](https://i.ibb.co/3WsFR5C/2.png)  

Из скриншотов видно, что большу часть heap памяти съедает наш MemoryGrabber, а в stackTrace мы видим Culprit т.к. именно на нем вся оставшаяся память была израсходована. 
Так как получилась OOM и почему stackTrace указывает на Culprit?
Само по себе возникновение OOM в каком-то из потоков ещё не означает, что именно этот поток «занял» всю свободную память, да и вообще не означает, что именно тот кусок кода, который привёл к OOM, виноват в этом.
Вполне нормальна ситуация, когда какой-то поток чем-то занимался, расходуя память, «дозанимался» этим до состояния «почти ничего не осталось», и завершил выполнение, приостановившись. А в это время какой-то другой поток решил запросить для своей маленькой работы ещё немного памяти, сборщик мусора постарался, конечно, но мусора уже в памяти не нашёл. В этом случае как раз и возникает OOM, не связанный с источником проблемы, когда стектрейс покажет совсем не того виновника падения приложения.


## Инструкция по запуску

# Необходимые инструменты:
[Maven](https://maven.apache.org/download.cgi)  
[Гайд по установке Maven](https://www.examclouds.com/ru/java/java-core-russian/install-maven)  
[JDK 8+](https://www.oracle.com/java/technologies/javase/javase8u211-later-archive-downloads.html)  
[Переменные среды JDK](https://java.com/ru/download/help/path_ru.html)  

# Запуск (Запускать ТОЛЬКО через скрипты, не через IDE)

Запустить `run.sh` или `run-windows.bat`  
```
mvn clean package
java -Xmx5m -Xms4m -XX:-UseGCOverheadLimit -XX:+HeapDumpOnOutOfMemoryError -jar target/WhosFault-1.0-SNAPSHOT.jar
$SHELL
```

`-Xmx5m` - set maximum Java heap size  
`-Xms4m` - set initial Java heap size  
`-XX:-UseGCOverheadLimit` -  необходимо чтобы получить именно `java.lang.OutOfMemoryError: Java heap space`, а не `java.lang.OutOfMemoryError: GC overhead limit exceeded`   
`-XX:+HeapDumpOnOutOfMemoryError` - данный флаг отвечает за создание дампа heap памяти в случае возникновения OOM  