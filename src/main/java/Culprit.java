import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class Culprit {
    private Map<Integer, String> temp;
    private static final Logger logger = Logger.getLogger(Culprit.class.getName());

    Culprit(){
        this.temp = new HashMap<>();
    }

      void init(){
        int iter = 0;
        while (iter != 200){
            this.temp.put(iter, "This " + iter + " -iteration");
            logger.info("Culprit init " + iter + "_iteration");
            iter++;
        }
    }
}
