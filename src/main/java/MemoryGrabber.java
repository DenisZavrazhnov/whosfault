import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

public class MemoryGrabber {
    private List<Object[]> array;
    private static final Logger logger = Logger.getLogger(MemoryGrabber.class.getName());

    MemoryGrabber(){
    	this.array = new LinkedList<>();
    }
    
    void grabMemory(){
        int capacity = 8500;
        for(int i = 0; i <= capacity; i++) {
            array.add(new Object[100]);
            logger.info("grabMemory " + i + "_iteration");
        }
    }
}
