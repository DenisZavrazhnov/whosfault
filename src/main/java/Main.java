public class Main {
    public static void main(String[] args) {
        MemoryGrabber memoryGrabber = new MemoryGrabber();
        memoryGrabber.grabMemory();
        Culprit culprit = new Culprit();
        culprit.init();
    }
}
